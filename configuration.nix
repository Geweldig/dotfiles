{
  pkgs,
  nixos-hardware,
  ...
}:
{
  imports = [
    nixos-hardware.nixosModules.framework-12th-gen-intel
    ./hardware-configuration.nix
    ./laptop-configuration.nix
    ./programs.nix
    ./services.nix
    ./session.nix
    ./users.nix
    ./home/main.nix
  ];

  boot = {
    tmp = {
      useTmpfs = true;
    };
    loader = {
      systemd-boot.enable = true;
      timeout = 1;
      efi.canTouchEfiVariables = true;
    };
    kernelPackages = pkgs.unstable.linuxPackages_zen;
    kernelModules = [ "tcp_bbr" ];
    kernel = {
      sysctl = {
        "net.ipv4.tcp_congestion_control" = "bbr";
        "net.core.default_qdisc" = "fq";
      };
    };
  };

  zramSwap.enable = true;

  networking = {
    hostName = "genco";
    networkmanager = {
      enable = true;
      wifi.powersave = false;
    };
    nameservers = [
      "1.1.1.1"
      "1.0.0.1"
      "2606:4700:4700::1111"
      "2606:4700:4700::1001"
    ];
  };
  systemd.services.ModemManager.enable = false;

  time.timeZone = "Europe/Amsterdam";

  i18n = {
    defaultLocale = "en_GB.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS = "en_GB.UTF-8";
      LC_IDENTIFICATION = "en_GB.UTF-8";
      LC_MEASUREMENT = "en_GB.UTF-8";
      LC_MONETARY = "en_GB.UTF-8";
      LC_NAME = "en_GB.UTF-8";
      LC_NUMERIC = "en_GB.UTF-8";
      LC_PAPER = "en_GB.UTF-8";
      LC_TELEPHONE = "en_GB.UTF-8";
      LC_TIME = "en_GB.UTF-8";
    };
  };

  system.stateVersion = "24.11";
  nix.settings = {
    auto-optimise-store = true;
    experimental-features = [
      "nix-command"
      "flakes"
    ];
  };
}
