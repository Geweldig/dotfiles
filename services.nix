{ config, pkgs, ... }:
{
  services = {
    blueman.enable = true;
    dbus.enable = true;
    getty.autologinUser = "bram";

    fwupd = {
      enable = true;
      extraRemotes = [ "lvfs-testing" ];
    };
    mullvad-vpn = {
      enable = true;
      package = pkgs.mullvad-vpn;
    };
    pipewire = {
      enable = true;
      alsa.enable = true;
      pulse.enable = true;
    };
    resolved = {
      enable = true;
      dnssec = "true";
      domains = [ "~." ];
      fallbackDns = config.networking.nameservers;
      extraConfig = ''
        DNSOverTLS=yes
      '';
    };
    udev.packages = with pkgs; [
      (writeTextFile {
        name = "bluetooth_power_save_udev";
        text = ''
          ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="8087", ATTR{idProduct}=="0032", ATTR{power/autosuspend}="-1"
        '';
        destination = "/etc/udev/rules.d/50-usb_power_save.rules";
      })
      android-udev-rules
    ];
    xserver.xkb = {
      layout = "us";
      variant = "";
    };
  };
}
