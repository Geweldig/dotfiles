{
  pkgs,
  ...
}:

{
  programs.sway = {
    enable = true;
    wrapperFeatures.gtk = true;
    extraPackages = [ ];
  };

  programs.river = {
    enable = true;
    package = pkgs.unstable.river;
    extraPackages = with pkgs; [
      swaybg

      unstable.kanshi
      unstable.ristate
      unstable.wideriver
    ];
  };

  environment.systemPackages = with pkgs; [
    adwaita-icon-theme
    breeze-icons
    deepin.deepin-gtk-theme
    dunst
    glib
    gnome-icon-theme
    keychain
    libnotify
    lxqt.lxqt-policykit
    pamixer
    pavucontrol
    sway-contrib.grimshot
    swayidle
    swaylock
    waybar
    wev
    wl-clipboard
    xdg-utils
  ];
  environment.sessionVariables.NIXOS_OZONE_WL = "1";

  systemd.user.services = {
    mpris-proxy = {
      description = "Mpris proxy";
      after = [
        "network.target"
        "sound.target"
      ];
      wantedBy = [ "default.target" ];
      serviceConfig.ExecStart = "${pkgs.bluez}/bin/mpris-proxy";
    };
  };

  xdg.portal = {
    enable = true;
    wlr.enable = true;
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  };

  fonts.packages = with pkgs; [
    (nerdfonts.override { fonts = [ "Lilex" ]; })
    corefonts
    dejavu_fonts
    font-awesome
    roboto
    vistafonts
  ];

  hardware = {
    bluetooth = {
      enable = true;
      powerOnBoot = false;
      settings = {
        General = {
          Enable = "Source,Sink,Media,Socket";
          Experimental = true;
        };
      };
    };
    flipperzero.enable = true;
    graphics = {
      enable = true;
      extraPackages = with pkgs; [
        intel-media-driver
        vaapiVdpau
        libvdpau-va-gl
      ];
    };
    pulseaudio.extraConfig = "
      load-module module-switch-on-connect
    ";
    enableAllFirmware = true;
  };

  security = {
    doas = {
      enable = true;
      extraRules = [
        {
          users = [ "bram" ];
          keepEnv = true;
        }
      ];
    };
    pam.services = {
      swaylock = { };
      swaylock.fprintAuth = true;
      doas.fprintAuth = true;
      login.fprintAuth = true;
    };
    polkit.enable = true;
    sudo.enable = false;
  };
}
