require'nvim-treesitter.configs'.setup {
    ensure_installed = { 
        "bash",
        "fish",
        "gitcommit",
        "gitignore",
        "lua",
        "nix",
        "python",
        "rust",
        "vim",
    },
    auto_install = true,
    highlight = {
        enable = true,
    },
}
