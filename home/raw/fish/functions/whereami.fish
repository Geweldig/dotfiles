function whereami -d "Get the current country according to ip-api.com"
    curl -s http://ip-api.com/json | jq -r '.country'
end
