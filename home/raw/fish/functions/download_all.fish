function download_all -a file -d "download all links in a certain file"
    xargs -n 1 curl -LO <$argv
end
