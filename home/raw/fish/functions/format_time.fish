function format_time -d "Format millis to Hour:Minute:Second"
    set -l days (math -s0 (math $argv[1] / (math '1000 * 60 * 60')) / 24)
    set -l hours (math -s0 (math $argv[1] / (math '1000 * 60 * 60')) % 24)
    set -l minutes (math -s0 (math $argv[1] / (math '1000 * 60')) % 60)
    set -l seconds (math -s0 (math $argv[1] / 1000) % 60)

    set -l days_string ""
    if test $days -gt 0
        set days_string $days"d:"
    end
    set -l hours_string ""
    if test $hours -gt 0 || test $days_string
        set hours_string (printf %02d $hours)
        set hours_string $hours_string"h:"
    end
    set -l minutes_string ""
    if test $minutes -gt 0 || test $hours_string
        set minutes_string (printf %02d $minutes)
        set minutes_string $minutes_string"m:"
    end
    set -l seconds_string ""
    if test $seconds -gt 0 || test $minutes_string
        set seconds_string (printf %02d $seconds)
        set seconds_string $seconds_string"s"
    end

    echo $days_string$hours_string$minutes_string$seconds_string
end
