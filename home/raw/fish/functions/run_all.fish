function run_all -d "run a command in all subfolders of current folder"
    if test (count $argv) -eq 0
        return 1
    end
    find . -maxdepth 1 -mindepth 1 -type d -exec fish -c "cd '{}' && echo 'Running for {}' && $argv && echo -e''" \;
end
