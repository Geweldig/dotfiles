function clippaste --wraps='wl-paste -n' --description 'alias clippaste=wl-paste -n'
    wl-paste -n $argv
end
