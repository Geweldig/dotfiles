function update -d "update all installed programs" -a force
    ping -c1 1.1.1.1 -W1 >/dev/null; or return
    if type -q nixos-rebuild
        nix flake update --flake ~/dotfiles
    end
    if type -q paru
        update_mirrors "$force"; or return
        paru; or return
        paru -c; or return
    end
    if type -q dnf
        sudo dnf upgrade --refresh; or return
        sudo dnf autoremove; or return
    end
    if type -q rustup
        rustup update; or return
    end
    if type -q cargo; and test -d ~/.cargo/bin
        cargo install-update -a; or return
    end
    if type -q nvim
        nvim +PlugUpgrade +PlugUpdate +PlugClean +qall; or return
    end
    if type -q flatpak
        flatpak update; or return
        flatpak uninstall --unused; or return
    end
end
