function music --wraps=ncmpcpp --description 'alias music ncmpcpp'
    ncmpcpp $argv 2>/dev/null
end
