function auto_fwupd --description 'run all the necessary fwupdmgr commands in sequence'
    doas fwupdmgr refresh
    doas fwupdmgr get-updates; or return
    doas fwupdmgr update; or return
end
