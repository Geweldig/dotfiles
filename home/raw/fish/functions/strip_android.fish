function strip_android --description 'strip all non-ascii characters from file names'
    rename --verbose 's/([^\x00-\x7F]|[\?\:\;\*<>\/\\\|"])//g' $argv *
end
