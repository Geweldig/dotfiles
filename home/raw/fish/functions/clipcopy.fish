function clipcopy --wraps='wl-copy -n' --description 'alias clipcopy=wl-copy -n'
    wl-copy -n $argv
end
