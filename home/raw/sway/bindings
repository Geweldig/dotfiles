# Close current window
bindsym $mod+Shift+q \
    kill

# Kill current application
bindsym $mod+Shift+alt+q \
    exec ${HOME}/.config/sway/scripts/kill

# Start launcher
bindsym $mod+d \
    exec $menu

# Reload the configuration file
bindsym $mod+Shift+c \
    reload

# Go to next workspace
bindsym $mod+Tab \
    workspace next

# Go to previous workspace
bindsym $mod+Shift+Tab \
    workspace prev

# Toggle bar
bindsym $mod+Shift+grave \
    exec killall -SIGUSR1 waybar

# Move focus left
bindsym $mod+h \
    focus left

# Move focus down
bindsym $mod+j \
    focus down

# Move focus up
bindsym $mod+k \
    focus up

# Move focus right
bindsym $mod+l \
    focus right

# Move window left
bindsym $mod+Shift+h \
    move left

# Move window down
bindsym $mod+Shift+j \
    move down

# Move window up
bindsym $mod+Shift+k \
    move up

# Move window left
bindsym $mod+Shift+l \
    move right

# Switch to workspace 1
bindsym $mod+1 \
    workspace number 1

# Switch to workspace 2
bindsym $mod+2 \
    workspace number 2

# Switch to workspace 3
bindsym $mod+3 \
    workspace number 3

# Switch to workspace 4
bindsym $mod+4 \
    workspace number 4

# Switch to workspace 5
bindsym $mod+5 \
    workspace number 5

# Switch to workspace 6
bindsym $mod+6 \
    workspace number 6

# Switch to workspace 7
bindsym $mod+7 \
    workspace number 7

# Switch to workspace 8
bindsym $mod+8 \
    workspace number 8

# Switch to workspace 9
bindsym $mod+9 \
    workspace number 9

# Switch to workspace 10
bindsym $mod+0 \
    workspace number 10

# Move focused container to workspace 1
bindsym $mod+Shift+1 \
    move container to workspace number 1

# Move focused container to workspace 2
bindsym $mod+Shift+2 \
    move container to workspace number 2

# Move focused container to workspace 3
bindsym $mod+Shift+3 \
    move container to workspace number 3

# Move focused container to workspace 4
bindsym $mod+Shift+4 \
    move container to workspace number 4

# Move focused container to workspace 5
bindsym $mod+Shift+5 \
    move container to workspace number 5

# Move focused container to workspace 6
bindsym $mod+Shift+6 \
    move container to workspace number 6

# Move focused container to workspace 7
bindsym $mod+Shift+7 \
    move container to workspace number 7

# Move focused container to workspace 8
bindsym $mod+Shift+8 \
    move container to workspace number 8

# Move focused container to workspace 9
bindsym $mod+Shift+9 \
    move container to workspace number 9

# Move focused container to workspace 10
bindsym $mod+Shift+0 \
    move container to workspace number 10

# Split window horizontally
bindsym $mod+apostrophe \
    splith

# Split window veritcally
bindsym $mod+semicolon \
    splitv

# Unsplit window
bindsym $mod+Return \
    split none

# Switch to stacking layout
bindsym $mod+w \
    layout stacking

# Switch to tiling layout
bindsym $mod+e \
    layout toggle split

# Switch to fullscreen
bindsym $mod+f \
    fullscreen

# Toggle the current focus between tiling and floating mode
bindsym $mod+s \
    floating toggle

# Swap focus between the tiling area and the floating area
bindsym $mod+space \
    focus mode_toggle

# Move focus to the parent container
bindsym $mod+p \
    focus parent

# Move the currently focused window to the scratchpad
bindsym $mod+Shift+minus \
    move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window
bindsym $mod+minus \
    scratchpad show

# Open a terminal
bindsym Control+Alt+t \
    exec alacritty

# Open firefox
bindsym Control+Alt+f \
    exec MOZ_ENABLE_WAYLAND=1 firefox

# Open a private firefox window
bindsym Control+Alt+p \
    exec MOZ_ENABLE_WAYLAND=1 firefox -private-window

# Open telegram
bindsym Control+Alt+r \
    exec telegram-desktop

# Open chat applications
bindsym Control+Alt+c \
    exec ${HOME}/.start_chat

# Open IntelliJ
bindsym Control+Alt+i \
    exec gtk-launch idea

# Take a screenshot of the screen
bindsym Print \
    exec grimshot copy screen

# Take a screenshot of an area
bindsym Shift+Print \
    exec grimshot copy area

# Take a screenshot of an area
bindsym Shift+$mod+s \
    exec grimshot copy area

# Take a screenshot of the current window
bindsym Control+Print \
    exec grimshot copy active

# Take a screenshot of a window
bindsym Shift+Control+Print \
    exec grimshot copy window

# Close notification
bindsym Control+space \
    exec dunstctl close

# Show notification history
bindsym Control+grave \
    exec dunstctl history-pop

# Increase brightness by 10
bindsym XF86MonBrightnessUp \
    exec light -A 10 && ${HOME}/.config/wm-scripts/notify_brightness

# Increase brightness by 1
bindsym Shift+XF86MonBrightnessUp \
    exec light -A 1 && ${HOME}/.config/wm-scripts/notify_brightness

# Set brightness to max
bindsym Control+XF86MonBrightnessUp \
    exec light -S 100 && ${HOME}/.config/wm-scripts/notify_brightness

# Decrease brightness by 10
bindsym XF86MonBrightnessDown \
    exec light -U 10 && ${HOME}/.config/wm-scripts/notify_brightness

# Decrease brightness by 1
bindsym Shift+XF86MonBrightnessDown \
    exec light -U 1 && ${HOME}/.config/wm-scripts/notify_brightness

# Set brightness to min
bindsym Control+XF86MonBrightnessDown \
    exec light -S 1 && ${HOME}/.config/wm-scripts/notify_brightness

# Decrease volume by 5
bindsym XF86AudioLowerVolume \
    exec pamixer -d 5 && ${HOME}/.config/wm-scripts/notify_sound

# Decrease volume by 1
bindsym Shift+XF86AudioLowerVolume \
    exec pamixer -d 1 && ${HOME}/.config/wm-scripts/notify_sound

# Set volume to min
bindsym Control+XF86AudioLowerVolume \
    exec pamixer -d 100 && ${HOME}/.config/wm-scripts/notify_sound

# Increase volume by 5
bindsym XF86AudioRaiseVolume \
    exec pamixer -i 5 && ${HOME}/.config/wm-scripts/notify_sound

# Incrase volume by 1
bindsym Shift+XF86AudioRaiseVolume \
    exec pamixer -i 1 && ${HOME}/.config/wm-scripts/notify_sound

# Set volume to max
bindsym Control+XF86AudioRaiseVolume \
    exec pamixer -i 100 && ${HOME}/.config/wm-scripts/notify_sound

# (Un)mute audio
bindsym XF86AudioMute \
    exec pamixer -t && ${HOME}/.config/wm-scripts/notify_mute

# Lock screen
bindsym $mod+Shift+w \
    exec $lock

# Play/pause music
bindsym XF86AudioPlay \
    exec playerctl play-pause

# Play/pause music
bindsym XF86AudioPause \
    exec playerctl play-pause

# Stop music
bindsym XF86AudioStop \
    exec playerctl stop

# Go to previous audio track
bindsym XF86AudioPrev \
    exec playerctl previous

# Go to next audio track
bindsym XF86AudioNext \
    exec playerctl next

# Show help
bindsym $mod+F1 \
    exec ${HOME}/.config/sway/scripts/show_help

# Show info
bindsym $mod+F2 \
    exec ${HOME}/.config/wm-scripts/show_info

# Pause notifications
bindsym $mod+F5 \
    exec dunstctl set-paused toggle

# Enable resize mode
bindsym $mod+r \
    mode 'resize'

mode "resize" {
    bindsym h resize shrink width 10px
    bindsym j resize grow height 10px
    bindsym k resize shrink height 10px
    bindsym l resize grow width 10px

    bindsym Shift+h resize set width 50ppt
    bindsym Shift+j resize set height 50ppt
    bindsym Shift+k resize set height 50ppt
    bindsym Shift+l resize set width 50ppt

    bindsym Return mode "default"
    bindsym Escape mode "default"
}
