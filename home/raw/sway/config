#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#=# Variables

set $mod Mod4
set $term alacritty
set $menu "${HOME}/.config/wm-scripts/menu"
set $lock "${HOME}/.config/wm-scripts/lock"

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#=# Output

output * bg "${HOME}/Pictures/wallpaper.jpg" fill
output 'BOE 0x095F Unknown' scale 1.2
output 'BOE 0x0BCA Unknown' scale 1.2
exec_always "${HOME}/.config/wm-scripts/import-gsettings"
exec swayidle -w \
         timeout 300 "$lock" \
         timeout 305 'swaymsg "output * power off"' \
         resume 'swaymsg "output * power on"' \
         timeout 5 'if pgrep -x swaylock; then swaymsg "output * power off"; fi' \
         resume 'swaymsg "output * power on"' \
         before-sleep "$lock"

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#=# Input configuration

input * {
    xkb_layout "us,us(dvorak)"
    xkb_options "ctrl:nocaps,compose:ralt,grp:ctrls_toggle"
}

input '2362:628:PIXA3854:00_093A:0274_Touchpad' {
    dwt           disabled
    accel_profile adaptive
    pointer_accel 0
    click_method  clickfinger
}

input '1133:45095:ERGO_M575_Mouse' {
    pointer_accel      -0.2
    scroll_method      on_button_down
    scroll_button      button3
    middle_emulation   true
}

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#=# Launch programs
exec dunst
exec lxqt-policykit-agent
exec 'systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP'
exec blueman-applet
exec xsettingsd

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#=# Key bindings

focus_follows_mouse no
# mouse_warping container
floating_modifier $mod normal

include bindings

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#=# Theme

seat * xcursor_theme Adwaita 24

# class                 border  bground text    indicator child_border
client.focused          #404552 #443446 #FFFFFF #F53C3C   #D4D4DA
client.focused_inactive #404552 #221923 #FFFFFF #F53C3C   #404552
client.unfocused        #404552 #726873 #FFFFFF #D4D5DA   #404552
client.urgent           #404552 #F53C3C #FFFFFF #F53C3C   #F53C3C
client.placeholder      #404552 #0C0C0C #FFFFFF #0C0C0C   #0C0C0C

client.background       #FFFFFF

# Gaps
gaps inner 4
gaps outer -4
for_window [class=".*"] border pixel 1
smart_gaps on
smart_borders on

for_window [title="(?:Open|Save) (?:File|Folder|As)"] floating enable;
for_window [title="TelegramDesktop"] floating enable, move position cursor;
for_window [window_role="pop-up"] floating enable;
for_window [window_role="bubble"] floating enable;
for_window [window_role="task_dialog"] floating enable;
for_window [window_role="Preferences"] floating enable;
for_window [window_type="dialog"] floating enable;
for_window [window_type="menu"] floating enable;
for_window [app_id = "xdg-desktop-portal-gtk"] floating enable;
for_window [app_id = "nm-connection-editor"] floating enable;
for_window [app_id = "pavucontrol"] floating enable;
for_window [app_id = "blueman-manager"] floating enable;

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#=# Status Bar

bar {
    swaybar_command waybar
}

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#=# Imports

include /etc/sway/config.d/*
