{
  config,
  ...
}:
{
  home-manager.users.bram = {
    home.file.".proxmark3/data/.gitkeep".text = "";
    home.file.".proxmark3/preferences.json".text = ''
      {
        "Created": "proxmark3",
        "FileType": "settings",
        "show.emoji": "emoji",
        "show.hints": true,
        "output.dense": false,
        "os.supports.colors": true,
        "file.default.savepath": "${config.users.users.bram.home}/.proxmark3/data",
        "file.default.dumppath": "${config.users.users.bram.home}/.proxmark3/data",
        "file.default.tracepath": "${config.users.users.bram.home}/.proxmark3/data",
        "window.plot.xpos": 10,
        "window.plot.ypos": 30,
        "window.plot.hsize": 400,
        "window.plot.wsize": 800,
        "window.overlay.xpos": 10,
        "window.overlay.ypos": 490,
        "window.overlay.hsize": 200,
        "window.overlay.wsize": 800,
        "window.overlay.sliders": true,
        "client.debug.level": "off",
        "show.bar.mode": "value",
        "client.exe.delay": 0,
        "client.timeout": 0
      }
    '';
  };
}
