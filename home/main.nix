{ home-manager, ... }:
{
  imports = [
    home-manager.nixosModules.default
    ./dunst.nix
    ./gtk.nix
    ./mime.nix
    ./proxmark3.nix
    ./river.nix
    ./sway.nix
  ];

  home-manager.users.bram = {
    xdg.configFile = {
      "alacritty".source = ./raw/alacritty;
      "fish/completions".source = ./raw/fish/completions;
      "fish/conf.d".source = ./raw/fish/conf.d;
      "fish/config.fish".source = ./raw/fish/config.fish;
      "fish/functions".source = ./raw/fish/functions;
      "fuzzel".source = ./raw/fuzzel;
      "mpv".source = ./raw/mpv;
      "nvim/init.vim".source = ./raw/nvim/init.vim;
      "nvim/plugin".source = ./raw/nvim/plugin;
      "waybar".source = ./raw/waybar;
      "wm-scripts".source = ./raw/wm-scripts;
      "xsettingsd".source = ./raw/xsettingsd;
    };

    home.file = {
      ".gitconfig".source = ./raw/gitconfig;
      ".shellcheckrc".source = ./raw/shellcheckrc;
      ".start_chat".source = ./raw/start_chat;
    };

    home.stateVersion = "24.11";
  };
}
