{
  config,
  lib,
  ...
}:
{
  home-manager.users.bram = {
    xdg.configFile = lib.mkIf (config.programs.sway.enable) { "sway".source = ./raw/sway; };
  };
}
